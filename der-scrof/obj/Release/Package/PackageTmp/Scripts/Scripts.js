﻿$(document).ready(function () {
    $('.matricula').mask('999999999');
    $('.cpf').mask('999.999.999-99');
    $('.cnpj').mask('99.999.999/9999-99');
    $('.data').mask('99/99/9999');
    $('.cep').mask('99999-999');
    $('.telefone').mask('(99) 9999-9999');
    $('.processo').mask('99-9999.*****-****/9999');    
    $('.numero-certificado').mask('9 999 999 999 999 999 9');
    $('.tipo-mascara-cpf').find('a').on('click', function () {
        $this = $(this);
        $('.txt-cpf-cnpj').mask($this.attr('data-mascara')).focus();
    });

    $('.txt-cpf-cnpj').css('width', '135px');

});