﻿using der_scrof.Models;
using der_scrof.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace der_scrof.Controllers
{
    [Authorize]
    public class UsuarioController : Controller
    {
        private der_db db = new der_db();

        // GET: /Usuario/Login/
        [AllowAnonymous]
        public ActionResult Login(UsuarioLogin model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                UsuarioLogin usuarioLogin = new UsuarioLogin();
                string senha = usuarioLogin.getUserPassword(model.MATRICULA);

                // Verifica se a senha está vazia!!!
                if (String.IsNullOrEmpty(senha))
                {
                    ModelState.AddModelError("", "A matrícula ou senha estão incorretos");
                }


                string senhaHash = Utils.HashSHA1(model.SENHA);

                if (senhaHash == senha.TrimEnd())
                {
                    FormsAuthentication.SetAuthCookie(model.MATRICULA, false);
                    if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/") && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                    {
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "A senha informada está incorreta");
                }
            }


            return View(model);
        }
        // GET: /Usuario/Logoff
        public ActionResult Logoff()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Usuario");
        }


    }
}
