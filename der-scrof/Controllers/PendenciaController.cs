﻿using der_scrof.Models;
using System;
using System.Linq;
using System.Web.Mvc;

namespace der_scrof.Controllers
{
    public class PendenciaController : Controller
    {
        private der_db db = new der_db();

        [HttpGet]
        public JsonResult Lista()
        {
            int EmpresaId = int.Parse(Request.QueryString["Id"]);
            var pendencias = db.Pendencia.Where(r => r.EmpresaId == EmpresaId && r.Active == true).Select(e =>
                new
                {
                    e.Id,
                    Pendencia = e.Observacao,
                    Situacao = e.Situacao.Descricao,
                    Validade = e.Validade,
                    e.Created,
                    setor = db.Usuario.FirstOrDefault(c => c.USUARIO_ID == e.CreatedBy).Setor.SIGLA,
                    e.EmpresaId
                }).ToList();

            var q = (from x in pendencias
                     select new
                     {
                         x.Id,
                         Pendencia = x.Pendencia,
                         x.setor,
                         x.Situacao,
                         x.EmpresaId,
                         Validade = x.Validade.ToString("dd/MM/yyyy"),
                         Data = x.Created.ToString("dd/MM/yyyy")
                     });
            return Json(q, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult ListaPorEngenheiro()
        {
            int EngenheiroId = int.Parse(Request.QueryString["Id"]);
            Engenheiro engenheiro = db.Engenheiro.Find(EngenheiroId);

            var pendencias = db.Pendencia.Where(r => r.PessoaId == engenheiro.PessoaId && r.Active == true).Select(e =>
                new
                {
                    e.Id,
                    Pendencia = e.Observacao,
                    Situacao = e.Situacao.Descricao,
                    Validade = e.Validade,
                    e.Created,
                    setor = db.Usuario.FirstOrDefault(c => c.USUARIO_ID == e.CreatedBy).Setor.SIGLA,
                    e.EmpresaId
                }).ToList();

            var q = (from x in pendencias
                     select new
                     {
                         x.Id,
                         Pendencia = x.Pendencia,
                         x.setor,
                         x.Situacao,
                         x.EmpresaId,
                         Validade = x.Validade.ToString("dd/MM/yyyy"),
                         Data = x.Created.ToString("dd/MM/yyyy")
                     });
            return Json(q, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult ListaAtivas()
        {
            int EmpresaId = int.Parse(Request.QueryString["Id"]);
            var pendencias = db.Pendencia.Where(r => r.SituacaoId == 1 && r.EmpresaId == EmpresaId && r.Active == true).Select(e =>
                new
                {
                    e.Id,
                    Pendencia = e.Observacao,
                    Situacao = e.Situacao.Descricao,
                    e.Created,
                    setor = db.Usuario.FirstOrDefault(c => c.USUARIO_ID == e.CreatedBy).Setor.SIGLA,
                    e.EmpresaId
                }).ToList();

            var q = (from x in pendencias
                     select new
                     {
                         x.Id,
                         x.Pendencia,
                         x.setor,
                         x.Situacao,
                         x.EmpresaId,
                         Data = x.Created.ToString("dd/MM/yyyy")
                     });
            return Json(q, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult QuantidadePendencias()
        {
            int EmpresaId = int.Parse(Request.QueryString["Id"]);
            var pendencias = db.Pendencia.Where(r => r.EmpresaId == EmpresaId && r.Situacao.Descricao != "Regular" && r.Situacao.Tipo == "Pedencia" && r.Active == true).ToList().Count;
            return Json(pendencias, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult QuantidadePendenciasEngenheiro()
        {
            int id = int.Parse(Request.QueryString["Id"]);
            var pendencias = db.Pendencia.Where(r => r.PessoaId == id && r.Situacao.Descricao != "Regular" && r.Situacao.Tipo == "Pedencia" && r.Active == true).ToList().Count;
            return Json(pendencias, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult Selecionar()
        {
            int PendenciaId = int.Parse(Request.QueryString["Id"]);
            var pendencia = db.Pendencia.Where(p => p.Id == PendenciaId && p.Active == true).Select(s => new
            {
                s.Id,
                s.Situacao.Descricao,
                s.Observacao,
                s.Empresa.RazaoSocial,
                s.Empresa.CNPJ_CPF,
                s.Created
            }).ToList();

            var tratado = (from s in pendencia
                           select new
                           {
                               s.Id,
                               s.Descricao,
                               s.Observacao,
                               s.RazaoSocial,
                               s.CNPJ_CPF,
                               Data = s.Created.ToString("dd/MM/yyyy")
                           });

            return Json(tratado.ToList(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Criar()
        {
            if (Request.Params["Id"] != null)
            {
                if (Request.Params["Id"] != "")
                {
                    int PendenciaId = int.Parse(Request.Params["Id"]);
                    int SituacaoId = int.Parse(Request.Params["Situacao"]);
                    string observacao = Request.Params["Observacao"];

                    observacao = observacao.Replace("\n", "<br>");

                    Usuario usuario = UsuarioLogin.getUserInfo();

                    Pendencia pendencia = db.Pendencia.SingleOrDefault(c => c.Id == PendenciaId);
                    if (pendencia != null)
                    {
                        pendencia.SituacaoId = SituacaoId;
                        pendencia.Observacao = observacao;
                        pendencia.Updated = DateTime.Now;
                        pendencia.UpdatedBy = usuario.USUARIO_ID;
                        pendencia.Validade = DateTime.Parse(Request.Params["Validade"]);
                    }
                }
                else if (Request.Params["EmpresaId"] != null)
                {
                    int EmpresaId = int.Parse(Request.Params["EmpresaId"]);
                    string observacao = Request.Params["Observacao"];
                    observacao = observacao.Replace("\n", "<br>");

                    Usuario usuario = UsuarioLogin.getUserInfo();

                    Pendencia pendencia = new Pendencia
                    {
                        Observacao = observacao,
                        EmpresaId = EmpresaId,
                        SituacaoId = 1, // pendente
                        Validade = DateTime.Parse(Request.Params["Validade"]),
                        Created = DateTime.Now,
                        CreatedBy = usuario.USUARIO_ID,
                        Active = true
                    };

                    db.Pendencia.Add(pendencia);
                }
                else if (Request.Params["EngenheiroId"] != null)
                {
                    //int EmpresaId = int.Parse(Request.Params["EmpresaId"]);

                    Pessoa pessoa = db.Engenheiro.Find(int.Parse(Request.Params["EngenheiroId"])).Pessoa;
                    string observacao = Request.Params["Observacao"];
                    observacao = observacao.Replace("\n", "<br>");

                    Usuario usuario = UsuarioLogin.getUserInfo();

                    Pendencia pendencia = new Pendencia
                    {
                        Observacao = observacao,
                        PessoaId = pessoa.Id,
                        SituacaoId = 1, // pendente
                        Validade = DateTime.Parse(Request.Params["Validade"]),
                        Created = DateTime.Now,
                        CreatedBy = usuario.USUARIO_ID,
                        Active = true
                    };

                    db.Pendencia.Add(pendencia);
                }
            }

            try
            {
                db.SaveChanges();
                return Json(true);
            }
            catch (Exception)
            {
                return Json(false);
            }
        }

        [HttpPost]
        public JsonResult Editar()
        {
            if (Request.Params["Id"] != null)
            {
                int PendenciaId = int.Parse(Request.Params["Id"]);
                int SituacaoId = int.Parse(Request.Params["Situacao"]);
                string observacao = Request.Params["Observacao"];

                Usuario usuario = UsuarioLogin.getUserInfo();

                Pendencia pendencia = db.Pendencia.SingleOrDefault(c => c.Id == PendenciaId);
                if (pendencia != null)
                {
                    pendencia.SituacaoId = SituacaoId;
                    pendencia.Observacao = observacao;
                    pendencia.Updated = DateTime.Now;
                    pendencia.UpdatedBy = usuario.USUARIO_ID;
                }
            }

            try
            {
                db.SaveChanges();
                return Json(true);
            }
            catch (Exception)
            {
                return Json(false);
            }
        }

        [HttpGet]
        public JsonResult Pesquisar()
        {
            if (Request.Params["Id"] != null)
            {
                int PendenciaId = int.Parse(Request.Params["Id"]);
                Pendencia pendencia = db.Pendencia.SingleOrDefault(c => c.Id == PendenciaId && c.Active == true);
                if (pendencia != null)
                {
                    var json = new
                    {
                        Id = pendencia.Id,
                        SituacaoId = pendencia.SituacaoId,
                        Observacao = pendencia.Observacao
                    };
                    return Json(json, JsonRequestBehavior.AllowGet);
                }
                return Json(0, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(0, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult Deletar()
        {
            if (Request.Params["Id"] != null)
            {
                int PendenciaId = int.Parse(Request.Params["Id"]);

                Usuario usuario = UsuarioLogin.getUserInfo();

                Pendencia pendencia = db.Pendencia.SingleOrDefault(c => c.Id == PendenciaId);
                if (pendencia != null)
                {
                    pendencia.Active = false;
                    pendencia.Updated = DateTime.Now;
                    pendencia.UpdatedBy = usuario.USUARIO_ID;
                }
            }

            try
            {
                db.SaveChanges();
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(false);
            }
        }

    }
}
