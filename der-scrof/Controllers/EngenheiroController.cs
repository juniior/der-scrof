﻿using der_scrof.Models;
using System;
using PagedList;
using System.Data;
using System.Linq;
using System.Web.Mvc;

namespace der_scrof.Controllers
{
    public class EngenheiroController : Controller
    {
        private der_db db = new der_db();

        [HttpPost]
        public JsonResult Registrar()
        {
            Usuario usuario = UsuarioLogin.getUserInfo();
            if (Request.Params["CPF"] != null)
            {
                string CPF = Request.Params["CPF"];
                int EmpresaId = int.Parse(Request.Params["EmpresaId"]);
                Engenheiro engenheiro = db.Engenheiro.SingleOrDefault(r => r.Pessoa.CPF == CPF && r.EmpresaId == EmpresaId);

                if (engenheiro != null)
                {
                    if (engenheiro.Active == true)
                    {
                        engenheiro.Pessoa.Nome = Request.Params["Nome"];
                        //engenheiro.Pessoa.CPF = Request.Params["CPF"];
                        engenheiro.Pessoa.Email = Request.Params["Email"];
                        engenheiro.Pessoa.Telefone = Request.Params["Telefone"];
                        engenheiro.Pessoa.Celular = Request.Params["Celular"];
                        //engenheiro.EmpresaId = int.Parse(Request.Params["EmpresaId"]);

                        engenheiro.Pessoa.Updated = DateTime.Now;
                        engenheiro.Pessoa.UpdatedBy = usuario.USUARIO_ID;

                        engenheiro.CREA = Request.Params["CREA"];
                        engenheiro.Visto = Request.Params["Visto"];
                        engenheiro.Updated = DateTime.Now;
                        engenheiro.UpdatedBy = usuario.USUARIO_ID;

                        db.Entry(engenheiro).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    else
                    {
                        return Json("Engenheiro desativado!");
                    }
                }
                else if (engenheiro == null)
                {
                    Pessoa pessoa = db.Pessoa.SingleOrDefault(p => p.CPF == CPF);
                    if (pessoa == null)
                    {
                        pessoa = new Pessoa
                        {
                            Nome = Request.Params["Nome"],
                            CPF = Request.Params["CPF"],
                            Email = Request.Params["Email"],
                            Telefone = Request.Params["Telefone"],
                            Celular = Request.Params["Celular"],
                            Created = DateTime.Now,
                            Active = true,
                            CreatedBy = usuario.USUARIO_ID
                        };

                        db.Pessoa.Add(pessoa);
                        db.SaveChanges();
                    }
                    else
                    {
                        pessoa.Nome = Request.Params["Nome"];
                        pessoa.Email = Request.Params["Email"];
                        pessoa.Celular = Request.Params["Celular"];
                        pessoa.Telefone = Request.Params["Telefone"];

                        pessoa.Updated = DateTime.Now;
                        pessoa.UpdatedBy = usuario.USUARIO_ID;

                        db.Entry(pessoa).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                    engenheiro = new Engenheiro
                    {
                        PessoaId = pessoa.Id,
                        EmpresaId = int.Parse(Request.Params["EmpresaId"]),
                        CREA = Request.Params["CREA"],
                        Visto = Request.Params["Visto"],
                        Created = DateTime.Now,
                        Active = true,
                        CreatedBy = usuario.USUARIO_ID
                    };

                    db.Engenheiro.Add(engenheiro);
                    db.SaveChanges();

                }
            }

            try
            {
                db.SaveChanges();
                return Json(true);
            }
            catch (Exception)
            {
                return Json(false);
            }
        }

        [HttpPost]
        public JsonResult Apagar()
        {
            if (Request.Params["Id"] != null)
            {
                Usuario usuario = UsuarioLogin.getUserInfo();
                int id = int.Parse(Request.Params["Id"]);

                Engenheiro engenheiro = db.Engenheiro.Find(id);
                if (engenheiro == null)
                {
                    return Json(false);
                }
                engenheiro.Active = false;
                engenheiro.Updated = DateTime.Now;
                engenheiro.UpdatedBy = usuario.USUARIO_ID;
            }
            try
            {
                db.SaveChanges();
                return Json(true);
            }
            catch (Exception)
            {
                return Json(false);
            }
        }

        [HttpGet]
        public JsonResult Lista()
        {
            int EmpresaId = int.Parse(Request.QueryString["Id"]);
            var engenheiros = db.Engenheiro.Where(r => r.EmpresaId == EmpresaId && r.Active == true).Select(e => new { e.Id, Engenheiro = e.Pessoa.Nome, e.CREA }
            ).ToList();

            return Json(engenheiros, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Ativar()
        {
            Usuario usuario = UsuarioLogin.getUserInfo();
            Engenheiro engenheiro = db.Engenheiro.Find(int.Parse(Request.Params["Id"]));
            if (engenheiro==null)
            {
                return Json(false);
            }

            engenheiro.Active = true;
            engenheiro.Updated = DateTime.Now;
            engenheiro.UpdatedBy = usuario.USUARIO_ID;

            try
            {
                db.SaveChanges();
                return Json(true);
            }
            catch (Exception)
            {
                return Json(false);
            }
        }

        [HttpGet]
        public JsonResult Pesquisar()
        {
            string CPF = Request.QueryString["CPF"];
            Engenheiro engenheiro;

            if (!string.IsNullOrEmpty(CPF))
            {
                int empresaId = int.Parse(Request.QueryString["EmpresaId"]);
                engenheiro = db.Engenheiro.SingleOrDefault(e => e.Pessoa.CPF == CPF && e.EmpresaId == empresaId && e.Active == true);
            }
            else
            {
                engenheiro = db.Engenheiro.Find(int.Parse(Request.QueryString["Id"]));
            }

            if (engenheiro != null)
            {
                var json = new
                {
                    Id = engenheiro.Id,
                    CPF = engenheiro.Pessoa.CPF,
                    Nome = engenheiro.Pessoa.Nome,
                    Email = engenheiro.Pessoa.Email,
                    Telefone = engenheiro.Pessoa.Telefone,
                    Celular = engenheiro.Pessoa.Celular,
                    CREA = engenheiro.CREA,
                    Visto = engenheiro.Visto
                };
                return Json(json, JsonRequestBehavior.AllowGet);
            }
            else
            {
                Pessoa pessoa = db.Pessoa.SingleOrDefault(p => p.CPF == CPF);
                if (pessoa != null)
                {
                    var json = new
                    {
                        Id = "",
                        CPF = pessoa.CPF,
                        Nome = pessoa.Nome,
                        Email = pessoa.Email,
                        Telefone = pessoa.Telefone,
                        Celular = pessoa.Celular,
                        CREA = "",
                        Visto = ""
                    };
                    return Json(json, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(0, JsonRequestBehavior.AllowGet);
                }
            }

        }

        public ActionResult Index(string sortOrder, string searchString, int? page)
        {
            //return View(db.Engenheiro.ToList().Where(x => x.Active == true));

            if (!(Request.HttpMethod == "GET"))
            {
                page = 1;
            }

            ViewBag.CurrentFilter = searchString;

            IQueryable<Engenheiro> engenheiros = db.Engenheiro.OrderBy(c => c.Pessoa.Nome);

            if (!String.IsNullOrEmpty(searchString))
            {
                engenheiros = engenheiros.Where(s => s.Pessoa.Nome.ToUpper().Contains(searchString.ToUpper()) || s.CREA.ToUpper().Contains(searchString.ToUpper()) || s.Pessoa.CPF.ToUpper().Contains(searchString.ToUpper()));
            }

            int pageSize = 15; // Registros por Página
            int pageNumber = (page ?? 1);

            return View(engenheiros.ToPagedList(pageNumber, pageSize));
        }

        public ActionResult Pendencias(int id)
        {
            Engenheiro engenheiro = db.Engenheiro.Find(id);
            if (engenheiro == null)
            {
                return HttpNotFound();
            }


            return View(engenheiro);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

    }
}
