﻿using der_scrof.Models;
using PagedList;
using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;

namespace der_scrof.Controllers
{
    public class EmpresaController : Controller
    {
        private der_db db = new der_db();

        // GET: /Empresa/
        public ActionResult Index(string sortOrder, string searchString, int? page)
        {
            if (!(Request.HttpMethod == "GET"))
            {
                page = 1;
            }

            ViewBag.CurrentFilter = searchString;

            IQueryable<Empresa> empresas = db.Empresa.OrderBy(c => c.RazaoSocial);

            if (!String.IsNullOrEmpty(searchString))
            {
                empresas = empresas.Where(s => s.RazaoSocial.ToUpper().Contains(searchString.ToUpper()) || s.CNPJ_CPF.ToUpper().Contains(searchString.ToUpper()));
            }

            int pageSize = 15; // Registros por Página
            int pageNumber = (page ?? 1);

            return View(empresas.ToPagedList(pageNumber, pageSize));
        }

        // GET: /Empresa/Procurar
        public ActionResult Procurar()
        {
            return View();
        }

        //POST: /Empresa/Procurar
        [HttpPost]
        public ActionResult Procurar(Empresa empresa)
        {
            string nu_cnpj = empresa.CNPJ_CPF;

            if (!string.IsNullOrEmpty(nu_cnpj) && nu_cnpj != "__.___.___/____-__" && nu_cnpj != "___.___.___-__")
            {
                empresa = db.Empresa.SingleOrDefault(c => c.CNPJ_CPF == nu_cnpj);

                if (empresa != null)
                {
                    return RedirectToAction("Editar", new { id = empresa.Id });
                }
                else
                {
                    TempData["AlertMessage"] = "Empresa com CNPJ/CPF " + nu_cnpj + " não localizado.";
                    TempData["AlertClass"] = "info";

                    TempData["ViewFormulario"] = true;
                    TempData["CNPJ_CPF"] = nu_cnpj;

                    return RedirectToAction("Create");
                }
            }
            else
            {
                TempData["AlertMessage"] = "Informe o CNPJ da Empresa";
                TempData["AlertClass"] = "error";
            }

            return View();
        }

        // GET: /Empresa/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Empresa/Create
        [HttpPost]
        public ActionResult Create(Empresa empresa)
        {
            if (ModelState.IsValid)
            {
                Usuario usuario = UsuarioLogin.getUserInfo();
                empresa.Created = DateTime.Now;
                empresa.CreatedBy = usuario.USUARIO_ID;
                empresa.RazaoSocial = empresa.RazaoSocial.ToUpper();

                string _Salt = ConfigurationManager.AppSettings["Salt"];
                string NovaSenha = empresa.CNPJ_CPF.Replace(".", "").Replace("/", "").Replace("-", "");
                string SenhaHash = FormsAuthentication.HashPasswordForStoringInConfigFile(_Salt.ToUpper() + NovaSenha, "sha1");

                empresa.Senha = SenhaHash;

                db.Empresa.Add(empresa);
                db.SaveChanges();

                TempData["AlertMessage"] = "Empresa registrada com sucessso!";
                TempData["CNPJ_CPF"] = empresa.CNPJ_CPF;

                return RedirectToAction("Procurar");
            }
            return View();
        }

        // GET: /Empresa/Editar
        public ActionResult Editar(int id = 0)
        {
            Empresa empresa = db.Empresa.Find(id);
            if (empresa == null)
            {
                return HttpNotFound();
            }

            ViewBag.SituacaoId = new SelectList(db.Situacao.Where(c => c.Tipo == "Pedencia").ToList(), "Id", "Descricao");

            int i = 0;
            if (empresa.Pendencia.Where(p => p.SituacaoId == 1 && p.Active == true).Count() > 0)
            {
                i++;
            }
            foreach (Engenheiro engenheiro in empresa.Engenheiro.ToList())
            {
                if (engenheiro.Pessoa.Pendencia.Where(p => p.SituacaoId == 1 && p.Active == true).Count() > 0)
                {
                    i++;
                }
            }

            if (i > 0)
            {
                ViewBag.Informacao = "Empresa com " + i + " pendências a este órgão.";
            }

            ViewBag.Certificados = (db.Certificado.Where(c => c.EmpresaId == empresa.Id).ToList());

            return View(empresa);
        }

        // POST: /Empresa/Editar/1
        [HttpPost]
        public ActionResult Editar(Empresa empresa)
        {
            if (ModelState.IsValid)
            {
                Usuario usuario = UsuarioLogin.getUserInfo();
                empresa.RazaoSocial = empresa.RazaoSocial.ToUpper();
                empresa.Updated = DateTime.Now;
                empresa.UpdatedBy = usuario.USUARIO_ID;

                db.Entry(empresa).State = EntityState.Modified;
                db.SaveChanges();

                TempData["AlertMessage"] = "Empresa alterada com sucessso!";
                TempData["CNPJ_CPF"] = empresa.CNPJ_CPF;
            }

            ViewBag.SituacaoId = new SelectList(db.Situacao.Where(c => c.Tipo == "Pedencia").ToList(), "Id", "Descricao");

            return View(empresa);
        }

    }
}
