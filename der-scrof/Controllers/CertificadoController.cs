﻿using der_scrof.Models;
using System;
using System.Linq;
using System.Web.Mvc;

namespace der_scrof.Controllers
{
    public class CertificadoController : Controller
    {
        private der_db db = new der_db();

        public ActionResult Consultar()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Consultar(Empresa empresa)
        {
            if (!string.IsNullOrEmpty(empresa.CNPJ_CPF))
            {
                Empresa empresaValida = db.Empresa.SingleOrDefault(d => d.CNPJ_CPF == empresa.CNPJ_CPF);
                if (empresaValida != null)
                {

                    int i = 0;
                    string motivo = "";
                    if (empresaValida.Pendencia.Where(p => p.SituacaoId == 1 && p.Active == true).Count() > 0)
                    {
                        motivo += "Empresa com pendências junto a este orgão. <br>";
                        i++;
                    }
                    foreach (Engenheiro engenheiro in empresaValida.Engenheiro.ToList())
                    {
                        if (engenheiro.Pessoa.Pendencia.Where(p => p.SituacaoId == 1 && p.Active == true).Count() > 0)
                        {
                            i++;
                            motivo += "Engenheiro " + engenheiro.Pessoa.Nome + " (" + engenheiro.Pessoa.CPF + ")" + " com pendências.<br>";
                        }
                    }
                    if (i > 0)
                    {
                        TempData["motivo"] = motivo;
                        TempData["empresa"] = empresaValida;

                        return RedirectToAction("Irregular");
                    }


                    Certificado certificado = new Certificado
                    {
                        DataEmissao = DateTime.Now,
                        DataValidade = DateTime.Now.AddDays(60),
                        NumeroCertificado = "",
                        Status = "Regular",
                        Empresa = empresaValida
                    };

                    db.Certificado.Add(certificado);
                    db.SaveChanges();

                    // 7 000 001 ddm myy yym mmm
                    // 7 000 001 101 220 125 8
                    // 7 - tipo de certificado
                    // 000 001 - numeração do documento (Id do Certificado)
                    // ddm myy yym mmm - data em que foi gerado

                    string numero = certificado.Id.ToString();

                    while (numero.Count() < 6)
                    {
                        numero = "0" + numero;
                    }

                    numero = "7" + numero + DateTime.Now.ToString("ddMMyyyymmmm");

                    certificado.NumeroCertificado = numero;

                    db.SaveChanges();

                    return RedirectToAction("Certidao", "Certificado", new { Id = certificado.Id.ToString() });
                }
            }

            return View();
        }

        public ActionResult Certidao(int? Id)
        {
            if (Id != null)
            {
                Certificado certificado = db.Certificado.SingleOrDefault(c => c.Id == Id);

                if (certificado != null)
                {
                    Empresa empresa = db.Empresa.SingleOrDefault(c => c.Id == certificado.Id);
                    ViewBag.Validade = certificado.DataValidade.ToShortDateString();
                    ViewBag.NumeroCertificado = certificado.NumeroCertificado;
                    ViewBag.RazaoSocial = certificado.Empresa.RazaoSocial;
                    ViewBag.CNPJ = certificado.Empresa.CNPJ_CPF;
                    ViewBag.IE = certificado.Empresa.IE;
                    ViewBag.Rua = certificado.Empresa.Rua;
                    ViewBag.Bairro = certificado.Empresa.Bairro;
                    ViewBag.Cidade = certificado.Empresa.Cidade;
                    ViewBag.UF = certificado.Empresa.UF;
                    ViewBag.DataHoje = DateTime.Now.ToLongDateString();
                }
            }

            return View();
        }

        public ActionResult Irregular(int? Id)
        {
            if (!string.IsNullOrEmpty(TempData["motivo"].ToString()))
            {
                ViewBag.Motivo = TempData["motivo"].ToString();
                ViewBag.Empresa = (Empresa)TempData["empresa"];
            }

            ViewBag.DataHoje = DateTime.Now.ToLongDateString();

            return View();
        }

        public ActionResult Validar()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Validar(Certificado certificado, string CNPJ_CPF)
        {
            Empresa empresa = db.Empresa.SingleOrDefault(e => e.CNPJ_CPF == CNPJ_CPF);
            Certificado cert = db.Certificado.SingleOrDefault(c => c.NumeroCertificado == certificado.NumeroCertificado.Replace(" ", "") && c.EmpresaId == empresa.Id);
            if (cert != null)
            {
                if (cert.DataValidade > DateTime.Now)
                {
                    ViewBag.tipo = "alert-success";
                    ViewBag.Mensagem = "Certificado valido até " + cert.DataValidade.ToString("dd/MM/yyyy.");
                }
                else
                {
                    ViewBag.tipo = "alert-error";
                    ViewBag.Mensagem = "Certificado expirado em " + cert.DataValidade.ToString("dd/MM/yyyy.");
                }
            }
            else
            {
                ViewBag.tipo = "";
                ViewBag.Mensagem = "Dados errados! Tente novamente!";
            }

            return View();
        }
    }
}
