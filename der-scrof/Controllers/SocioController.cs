﻿using der_scrof.Models;
using System;
using System.Data;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web.Mvc;

namespace der_scrof.Controllers
{
    public class SocioController : Controller
    {
        private der_db db = new der_db();

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        [HttpGet]
        public JsonResult Pesquisar()
        {
            string CPF = Request.QueryString["CPF"];

            Socio socio;

            if (!string.IsNullOrEmpty(CPF))
            {
                int empresaId = int.Parse(Request.QueryString["EmpresaId"]);
                socio = db.Socio.SingleOrDefault(e => e.Pessoa.CPF == CPF && e.EmpresaId == empresaId);
            }
            else
            {
                socio = db.Socio.Find(int.Parse(Request.QueryString["Id"]));
            }

            if (socio != null)
            {
                var json = new
                {
                    Id = socio.Id,
                    CPF = socio.Pessoa.CPF,
                    Nome = socio.Pessoa.Nome,
                    Email = socio.Pessoa.Email,
                    Telefone = socio.Pessoa.Telefone,
                    Celular = socio.Pessoa.Celular
                };
                return Json(json, JsonRequestBehavior.AllowGet);
            }
            else
            {
                Pessoa pessoa = db.Pessoa.SingleOrDefault(p => p.CPF == CPF);
                if (pessoa != null)
                {
                    var json = new
                    {
                        Id = "",
                        CPF = pessoa.CPF,
                        Nome = pessoa.Nome,
                        Email = pessoa.Email,
                        Telefone = pessoa.Telefone,
                        Celular = pessoa.Celular
                    };
                    return Json(json, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(0, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpPost]
        public JsonResult Registrar()
        {
            try
            {
                Usuario usuario = UsuarioLogin.getUserInfo();
                if (Request.Params["CPF"] != null)
                {
                    string CPF = Request.Params["CPF"];
                    int EmpresaId = int.Parse(Request.Params["EmpresaId"]);

                    Pessoa pessoa = db.Pessoa.SingleOrDefault(e => e.CPF == CPF);
                    Socio socio = db.Socio.SingleOrDefault(e => e.Pessoa.CPF == CPF && e.EmpresaId == EmpresaId);

                    if (socio == null && pessoa != null)
                    {
                        socio = new Socio
                        {
                            Created = DateTime.Now,
                            CreatedBy = usuario.USUARIO_ID,
                            EmpresaId = int.Parse(Request.Params["EmpresaId"]),
                            PessoaId = pessoa.Id
                        };

                        db.Socio.Add(socio);
                        db.SaveChanges();
                    }
                    else if (socio != null)
                    {
                        pessoa = socio.Pessoa;
                        pessoa.Nome = Request.Params["Nome"];
                        pessoa.Email = Request.Params["Email"];
                        pessoa.Celular = Request.Params["Celular"];
                        pessoa.Telefone = Request.Params["Telefone"];

                        pessoa.Updated = DateTime.Now;
                        pessoa.UpdatedBy = usuario.USUARIO_ID;

                        db.Entry(pessoa).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    else
                    {
                        pessoa = new Pessoa
                        {
                            CPF = CPF,
                            Nome = Request.Params["Nome"],
                            Email = Request.Params["Email"],
                            Telefone = Request.Params["Telefone"],
                            Celular = Request.Params["Celular"],
                            Active = true,
                            Created = DateTime.Now,
                            CreatedBy = usuario.USUARIO_ID
                        };

                        db.Pessoa.Add(pessoa);
                        db.SaveChanges();

                        socio = new Socio
                        {
                            Created = DateTime.Now,
                            CreatedBy = usuario.USUARIO_ID,
                            EmpresaId = int.Parse(Request.Params["EmpresaId"]),
                            PessoaId = pessoa.Id
                        };

                        db.Socio.Add(socio);
                        db.SaveChanges();
                    }

                }

                return Json(true);
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                    }
                }

                return Json(false);
            }
        }

        [HttpGet]
        public JsonResult Listar()
        {
            int EmpresaId = int.Parse(Request.QueryString["Id"]);
            var socios = db.Socio.Where(r => r.EmpresaId == EmpresaId).Select(e =>
                new { e.Id, Socio = e.Pessoa.Nome, e.Pessoa.CPF }
            ).ToList();

            return Json(socios, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Apagar()
        {
            if (Request.Params["Id"] != null)
            {
                int id = int.Parse(Request.Params["Id"]);

                Socio socio = db.Socio.Find(id);
                if (socio == null)
                {
                    return Json(false);
                }
                db.Socio.Remove(socio);
            }

            try
            {
                db.SaveChanges();
                return Json(true);
            }
            catch (Exception)
            {
                return Json(false);
            }
        }

    }
}
