﻿using der_scrof.Models;
using System;
using System.Linq;
using System.Web.Mvc;

namespace der_scrof.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            der_db db = new der_db();

            var pen = db.Pendencia.Where(p => p.SituacaoId == 1 && p.Validade < DateTime.Now && p.Active == true).ToList();
            
            foreach (var item in pen)
            {
                item.Active = false;
                item.Updated = DateTime.Now;
                item.UpdatedBy = 0;

                db.SaveChanges();
            }

            return View();
        }


    }
}
