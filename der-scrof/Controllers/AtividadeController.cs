﻿using der_scrof.Models;
using System;
using System.Linq;
using System.Web.Mvc;

namespace der_scrof.Controllers
{
    public class AtividadeController : Controller
    {
        private der_db db = new der_db();

        public JsonResult PesquisaAutoAtividades(string term)
        {
            var itens = db.Atividade.Select(s => new { Id = s.Codigo, value = s.Nome }).ToList().Where(c => c.value.Contains(term) || c.Id.ToString().Contains(term)).Select(s => new { s.Id, value = s.Id.ToString() + " - " + s.value }).Take(10).ToArray(); ;
            return Json(itens, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult Lista()
        {
            int EmpresaId = int.Parse(Request.QueryString["Id"]);
            var atividades = db.AtividadeEmpresa.Where(r => r.EmpresaId == EmpresaId).Select(e => new { e.Id, Atividade = e.Atividade.Nome, Codigo = e.Atividade.Codigo }).ToList();

            return Json(atividades, JsonRequestBehavior.AllowGet);
        }

    }
}
