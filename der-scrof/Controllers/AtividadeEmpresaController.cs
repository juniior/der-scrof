﻿using der_scrof.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace der_scrof.Controllers
{
    public class AtividadeEmpresaController : Controller
    {
        private der_db db = new der_db();

        [HttpPost]
        public JsonResult Deletar()
        {
            if (Request.Params["Id"] != null)
            {
                int id = int.Parse(Request.Params["Id"]);

                AtividadeEmpresa atividadeEmpresa = db.AtividadeEmpresa.Find(id);
                if (atividadeEmpresa == null)
                {
                    return Json(false);
                }
                db.AtividadeEmpresa.Remove(atividadeEmpresa);
            }

            try
            {
                db.SaveChanges();
                return Json(true);
            }
            catch (Exception)
            {
                return Json(false);
            }
        }

        [HttpPost]
        public JsonResult Registrar()
        {
            if (Request.Params["EmpresaId"] != null)
            {
                int CodigoAtividade = int.Parse(Request.Params["Id"]);
                int EmpresaId = int.Parse(Request.Params["EmpresaId"]);

                AtividadeEmpresa atividadeEmpresa = db.AtividadeEmpresa.SingleOrDefault(c => c.EmpresaId == EmpresaId && c.AtividadeId == CodigoAtividade);

                if (atividadeEmpresa == null)
                {
                    atividadeEmpresa = new AtividadeEmpresa
                    {
                        AtividadeId = CodigoAtividade,
                        EmpresaId = EmpresaId
                    };
                    db.AtividadeEmpresa.Add(atividadeEmpresa);
                }
            }
            try
            {
                db.SaveChanges();
                return Json(true);
            }
            catch (Exception)
            {
                return Json(false);
            }
        }

    }
}
