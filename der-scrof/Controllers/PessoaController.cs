﻿using der_scrof.Models;
using System.Linq;
using System.Web.Mvc;

namespace der_scrof.Controllers
{
    public class PessoaController : Controller
    {
        private der_db db = new der_db();

        [HttpGet]
        public JsonResult Pesquisar()
        {
            string cpf = Request.QueryString["CPF"];
            var pessoa = db.Pessoa.SingleOrDefault(c => c.CPF == cpf);
            if (pessoa != null)
            {
                var json = new
                {
                    Id = pessoa.Id,
                    Nome = pessoa.Nome
                };
                return Json(json, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        
        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

    }
}
