﻿using der_scrof.Models;
using System.Web.Mvc;
using System.Web.Routing;

namespace der_scrof.Filters
{
    public class AppFilter : ActionFilterAttribute
    {
        // Lorismar* - OnActionExecuting - Invocado pouco antes de um método Action ser executado
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            if (!UsuarioLogin.checkPermissao())
            {
                //if ((string)filterContext.RouteData.Values["controller"] != "Usuario" &&
                //    (string)filterContext.RouteData.Values["action"] != "Login")
                //{
                //    filterContext.Controller.TempData["AlertMessage"] = "O usuário não tem acesso a essa área.";
                //    filterContext.Controller.TempData["AlertClass"] = "info";

                //    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary { { "action", "Login" }, { "controller", "Usuario" } }
                //    );
                //}
            }
            else
            {
                Permissao permissao = UsuarioLogin.getUserPermission();
                if (permissao.Nivel != null)
                {
                    filterContext.Controller.TempData["Permissao"] = permissao.Nivel.DESCRICAO;
                }
                else
                {
                    filterContext.Controller.TempData["Permissao"] = null;
                }
            }
        }
        //  OnActionExecuted - Invocado logo após que execução de um método Action foi terminado
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);
        }
    }
}