﻿using System.Configuration;
using System.Web.Security;

namespace der_scrof.Util
{
    public class Utils
    {
        public static string HashSHA1(string s)
        {
            string _Salt = ConfigurationManager.AppSettings["Salt"];
            return FormsAuthentication.HashPasswordForStoringInConfigFile(_Salt.ToUpper() + s, "sha1");
        }
    }
}