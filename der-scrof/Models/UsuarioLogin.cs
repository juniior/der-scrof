﻿using der_scrof.Models;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace der_scrof.Models
{
    [Table("Usuario")]
    public class UsuarioLogin
    {
        private der_db db = new der_db();

        [Required(ErrorMessage = "Informe a matrícula")]
        [Display(Name = "Matrícula")]
        public string MATRICULA { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Informe a senha")]
        [Display(Name = "Senha")]
        public string SENHA { get; set; }

        public string getUserPassword(string matricula)
        {
            var usuario = db.Usuario.SingleOrDefault(u => u.MATRICULA == matricula);
            if (usuario != null)
            {
                return usuario.SENHA;
            }
            else
            {
                return String.Empty;
            }
        }

        public static Usuario getUserInfo()
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                if (HttpContext.Current.User.Identity.Name.Count() != 9)
                {
                    FormsAuthentication.SignOut();
                    return new Usuario();
                }
                der_db db = new der_db();
                return db.Usuario.Single(c => c.MATRICULA == HttpContext.Current.User.Identity.Name);
            }
            return new Usuario();
        }


        public static Permissao getUserPermission()
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                der_db db = new der_db();

                return db.Permissao.SingleOrDefault(p =>
                    p.Usuario.MATRICULA == HttpContext.Current.User.Identity.Name &&
                    p.Sistema.SIGLA == "SROF"
                );
            }
            else
            {
                return new Permissao();
            }
        }

        public static bool checkPermissao()
        {
            Permissao permissao = UsuarioLogin.getUserPermission();
            if (permissao != null)
            {
                if (permissao.Nivel != null)
                {
                    if (permissao.Nivel.DESCRICAO == "Administrador" || permissao.Nivel.DESCRICAO == "Gerente")
                    {
                        return true;
                    }
                }
            }

            return false;
        }

    }
}